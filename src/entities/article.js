const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ArticleSchema = new Schema({
  title: {
    type: String,
    validate: {
      validator: (title) => title.length >= 4,
      message: "Title must be longer than 4 characters",
    },
    required: [true, "Title is required"],
  },
  game: {
    type: Schema.Types.ObjectId,
    ref: "game",
    required: [true, "Game is required"],
  },
  content: {
    type: String,
    validate: {
      validator: (content) => content.length >= 50,
      message: "Content must be longer than 50 characters",
    },
    required: [true, "Content is required"],
  },
  postDate: Date,
  subjects: [String],
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: "user",
    required: [true, "User is required"],
  },
});

const Article = mongoose.model("article", ArticleSchema);

module.exports = Article;
