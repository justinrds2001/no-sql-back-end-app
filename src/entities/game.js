const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const GameSchema = new Schema({
  name: {
    type: String,
    validate: {
      validator: (name) => name.length >= 4,
      message: "Name must be longer than 4 characters",
    },
    required: [true, "Name is required"],
  },
  description: {
    type: String,
    validate: {
      validator: (description) => description.length >= 20,
      message: "Description must be longer than 20 characters",
    },
    required: [true, "Description is required"],
  },
  releaseDate: {
    type: Date,
    required: [true, "Release date is required"],
  },
  developer: {
    type: Schema.Types.ObjectId,
    ref: "developer",
    required: [true, "Developer is required"],
  },
  tags: [String],
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: "user",
    required: [true, "User is required"],
  },
});
const Game = mongoose.model("game", GameSchema);

module.exports = Game;
