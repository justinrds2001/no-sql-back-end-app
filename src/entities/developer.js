const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const DeveloperSchema = new Schema({
  name: {
    type: String,
    validate: {
      validator: (name) => name.length >= 4,
      message: "Name must be longer than 4 characters",
    },
    required: [true, "Name is required"],
  },
  headquartersLocation: {
    type: String,
    validate: {
      validator: (headquartersLocation) => headquartersLocation.length > 4,
      message: "Location must be longer than 4 characters",
    },
    required: [true, "Location is required"],
  },
  dateOfEstablishment: {
    type: Date,
    required: [true, "Date is required"],
  },
  founders: [
    {
      type: String,
    },
  ],
  website: {
    type: String,
    required: [true, "Website is required"],
  },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: "user",
    required: [true, "User is required"],
  },
});
const Developer = mongoose.model("developer", DeveloperSchema);

module.exports = Developer;
