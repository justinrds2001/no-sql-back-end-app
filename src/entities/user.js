const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  username: {
    type: String,
    validate: {
      validator: (username) => username.length >= 4,
      message: "Username must be longer than 4 characters",
    },
    required: [true, "Username is required"],
  },
  password: {
    type: String,
    validate: {
      validator: (password) => password.length >= 4,
      message: "Password must be longer than 4 characters",
    },
    required: [true, "Password is required"],
  },
});
const User = mongoose.model("user", UserSchema);
module.exports = User;
