const mongoose = require("mongoose");
const Developer = require("../entities/developer");
const logger = require("tracer").console();

module.exports = {
  create: (req, res, next) => {
    logger.log("create developer was called");
    // Do something with mongoose
    const developer = new Developer(req.body);
    developer
      .save()
      .then(() => {
        res.status(200).json({
          status: "successful",
          addedDeveloper: developer,
        });
      })
      .catch((err) => {
        next({ message: err.message, errorCode: 500 });
      });
  },

  read: (req, res, next) => {
    logger.log("read developers was called");
    // Do something with mongoose
    Developer.find({})
      .then((developers) => {
        res.status(200).json(developers);
      })
      .catch((err) => {
        next({ message: err.message, errorCode: 500 });
      });
  },

  readById: (req, res, next) => {
    logger.log("read developer by id was called");
    // Do something with mongoose
    const devId = req.params.developerId;
    Developer.findById(devId)
      .then((dev) => {
        res.status(200).json(dev);
      })
      .catch((err) => {
        next({ message: err.message, errorCode: 500 });
      });
  },

  update: (req, res, next) => {
    logger.log("update developer by id was called");
    // Do something with mongoose
    const devId = req.params.developerId;
    const newDev = req.body;
    Developer.findByIdAndUpdate(devId, newDev)
      .then((updatedDev) => {
        res.status(200).json({
          status: "successful",
          updatedDeveloper: updatedDev,
        });
      })
      .catch((err) => {
        next({ message: err.message, errorCode: 500 });
      });
  },

  delete: (req, res, next) => {
    logger.log("delete developer by id was called");
    // Do something with mongoose
    const devId = req.params.developerId;
    Developer.findByIdAndRemove(devId)
      .then((deletedDev) => {
        res.status(200).json({
          status: "successful",
          deletedDeveloper: deletedDev,
        });
      })
      .catch((err) => {
        next({ message: err.message, errorCode: 500 });
      });
  },
};
