const mongoose = require("mongoose");
const Article = require("../entities/article");
const logger = require("tracer").console();

module.exports = {
  create: (req, res, next) => {
    logger.log("create article was called");
    // Do something with mongoose
    const article = new Article(req.body);
    article
      .save()
      .then(() => {
        res.status(200).json({
          status: "successful",
          addedArticle: article,
        });
      })
      .catch((err) => {
        next({ message: err.message, errorCode: 500 });
      });
  },

  read: (req, res, next) => {
    logger.log("read articles was called");
    // Do something with mongoose
    Article.find({})
      .populate("game")
      .then((articles) => {
        res.status(200).json(articles);
      })
      .catch((err) => {
        next({ message: err.message, errorCode: 500 });
      });
  },

  readById: (req, res, next) => {
    logger.log("read article by id was called");
    // Do something with mongoose
    const articleId = req.params.articleId;
    Article.findById(articleId)
      .populate("game")
      .then((article) => {
        res.status(200).json(article);
      })
      .catch((err) => {
        next({ message: err.message, errorCode: 500 });
      });
  },

  update: (req, res, next) => {
    logger.log("update game by id was called");
    // Do something with mongoose
    const articleId = req.params.articleId;
    const newArticle = req.body;
    Article.findByIdAndUpdate(articleId, newArticle)
      .then((updatedArticle) => {
        res.status(200).json({
          status: "successful",
          updatedArticle: updatedArticle,
        });
      })
      .catch((err) => {
        next({ message: err.message, errorCode: 500 });
      });
  },

  delete: (req, res, next) => {
    logger.log("delete article by id was called");
    // Do something with mongoose
    const articleId = req.params.articleId;
    Article.findByIdAndRemove(articleId)
      .then((deletedArticle) => {
        res.status(200).json({
          status: "successful",
          deletedArticle: deletedArticle,
        });
      })
      .catch((err) => {
        next({ message: err.message, errorCode: 500 });
      });
  },
};
