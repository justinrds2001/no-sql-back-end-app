const mongoose = require("mongoose");
const Game = require("../entities/game");
const logger = require("tracer").console();

module.exports = {
  create: (req, res, next) => {
    logger.log("create game was called");
    // Do something with mongoose
    const game = new Game(req.body);
    game
      .save()
      .then(() => {
        res.status(200).json({
          status: "successful",
          addedGame: game,
        });
      })
      .catch((err) => {
        next({ message: err.message, errorCode: 500 });
      });
  },

  read: (req, res, next) => {
    logger.log("read games was called");
    // Do something with mongoose
    Game.find({})
      .then((games) => {
        res.status(200).json(games);
      })
      .catch((err) => {
        next({ message: err.message, errorCode: 500 });
      });
  },

  readById: (req, res, next) => {
    logger.log("read game by id was called");
    // Do something with mongoose
    const gameId = req.params.gameId;
    Game.findById(gameId)
      .populate("developer")
      .then((game) => {
        res.status(200).json(game);
      })
      .catch((err) => {
        next({ message: err.message, errorCode: 500 });
      });
  },

  update: (req, res, next) => {
    logger.log("update game by id was called");
    // Do something with mongoose
    const gameId = req.params.gameId;
    const newGame = req.body;
    Game.findByIdAndUpdate(gameId, newGame)
      .then((updatedGame) => {
        res.status(200).json({
          status: "successful",
          updatedGame: updatedGame,
        });
      })
      .catch((err) => {
        next({ message: err.message, errorCode: 500 });
      });
  },

  delete: (req, res, next) => {
    logger.log("delete game by id was called");
    // Do something with mongoose
    const gameId = req.params.gameId;
    Game.findByIdAndRemove(gameId)
      .then((deletedGame) => {
        res.status(200).json({
          status: "successful",
          deletedGame: deletedGame,
        });
      })
      .catch((err) => {
        next({ message: err.message, errorCode: 500 });
      });
  },
};
