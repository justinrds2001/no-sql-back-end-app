const mongoose = require("mongoose");
const User = require("../entities/user");
const logger = require("tracer").console();
const jwt = require("jsonwebtoken");
const jwtSecretKey = require("../config/config").jwtSecretKey;

module.exports = {
  register: async function (req, res, next) {
    console.log("register");
    const user = await User.findOne({ username: req.body.username });

    if (user) {
      console.log(
        "A user with username: ",
        req.body.username,
        " already exists"
      );
      console.log(user);
      next({ message: "Username is already taken", errorCode: 500 });
    } else {
      const created = new User(req.body);
      await created.save();
      if (created.isNew) {
        next({ message: "Username is already taken", errorCode: 500 });
      } else {
        const payload = {
          _id: created._id,
        };
        console.log("inserted id: " + created._id);
        console.log("user added");
        res.status(200).json({
          status: "successful",
          registeredUser: created,
          token: jwt.sign(payload, jwtSecretKey, { expiresIn: "2h" }),
        });
      }
    }
  },

  login: async function (req, res, next) {
    const { username, password } = req.body;
    const user = await User.findOne({ username: username });
    if (user) {
      if (user.password === password) {
        const payload = {
          _id: user._id,
        };
        res.status(200).json({
          status: "successful",
          loggedInUser: user,
          token: jwt.sign(payload, jwtSecretKey, { expiresIn: "2h" }),
        });
      } else {
        next({
          message: "User not found or password invalid",
          errorCode: 400,
        });
      }
    } else {
      next({
        message: "User not found or password invalid",
        errorCode: 400,
      });
    }
  },

  validateToken: (req, res, next) => {
    logger.info("validateToken called");
    // logger.trace(req.headers)
    // The headers should contain the authorization-field with value 'Bearer [token]'
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      logger.warn("Authorization header missing!");
      next({ message: "not authorized", errorCode: 401 });
    } else {
      // Strip the word 'Bearer ' from the headervalue
      const token = authHeader.substring(7, authHeader.length);

      jwt.verify(token, jwtSecretKey, (err, payload) => {
        if (err) {
          logger.warn("Not authorized");
          next({ message: "not authorized", errorCode: 401 });
        }
        if (payload) {
          logger.debug("token is valid", payload);
          // User has access. add UserId from payload to
          // request, for each next endpoint.
          req.userId = payload._id;
          next();
        }
      });
    }
  },

  validateTokenEndpoint: (req, res, next) => {
    logger.info("validateToken called");
    // logger.trace(req.headers)
    // The headers should contain the authorization-field with value 'Bearer [token]'
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      logger.warn("Authorization header missing!");
      next({ message: "not authorized", errorCode: 401 });
    } else {
      // Strip the word 'Bearer ' from the headervalue
      const token = authHeader.substring(7, authHeader.length);

      jwt.verify(token, jwtSecretKey, (err, payload) => {
        if (err) {
          logger.warn("Not authorized");
          next({ message: "not authorized", errorCode: 401 });
        }
        if (payload) {
          logger.debug("token is valid", payload);
          // User has access. add UserId from payload to
          // request, for each next endpoint.
          req.userId = payload._id;

          res.status(200).json({
            status: "successful",
            payload: payload,
          });
        }
      });
    }
  },
};
