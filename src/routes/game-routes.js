const express = require("express");
const router = express.Router();
const gameController = require("../controllers/game-controller");
const authenticationController = require("../controllers/authentication-controller");

router.route("/").get(gameController.read).post(gameController.create);

router
  .route("/:gameId")
  .get(gameController.readById)
  .put(gameController.update)
  .delete(gameController.delete);

module.exports = router;
