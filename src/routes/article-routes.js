const express = require("express");
const router = express.Router();
const articleController = require("../controllers/article-controller");

router.route("/").get(articleController.read).post(articleController.create);

router
  .route("/:articleId")
  .get(articleController.readById)
  .put(articleController.update)
  .delete(articleController.delete);

module.exports = router;
