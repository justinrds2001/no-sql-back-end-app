const express = require("express");
const logger = require("tracer").console();
const router = express.Router();
const authenticatorController = require("../controllers/authentication-controller");

router.post("/login", authenticatorController.login);
router.post("/register", authenticatorController.register);
router.get("/profile", authenticatorController.validateTokenEndpoint);

module.exports = router;
