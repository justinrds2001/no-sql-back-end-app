const express = require("express");
const router = express.Router();
const developerController = require("../controllers/developer-controller");

router
  .route("/")
  .get(developerController.read)
  .post(developerController.create);

router
  .route("/:developerId")
  .get(developerController.readById)
  .put(developerController.update)
  .delete(developerController.delete);

module.exports = router;
