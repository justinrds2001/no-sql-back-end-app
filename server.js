const express = require("express");
const app = express();
const port = process.env.PORT || 3000;
const mongoose = require("mongoose");
const gameRoutes = require("./src/routes/game-routes");
const developerRoutes = require("./src/routes/developer-routes");
const articleRoutes = require("./src/routes/article-routes");
const authenticationRoutes = require("./src/routes/authentication-routes");

// Setting up default mongoose connection
var mongoDB =
  "mongodb+srv://Justinrds2001:Origami0303@games-database.hj0rp.mongodb.net/mongoGameDatabase?retryWrites=true&w=majority";
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

const db = mongoose.connection;

db.on("error", console.error.bind(console, "MongoDB connection error:"));

const logger = require("tracer").console();
app.use(express.json());

// Add CORS headers
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type,authorization"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

// logger
app.all("*", (req, res, next) => {
  logger.log("Endpoint called: " + req.method + " " + req.url);
  next();
});

// Routing should be placed here
app.use("/api/game", gameRoutes);
app.use("/api/developer", developerRoutes);
app.use("/api/article", articleRoutes);
app.use("/api", authenticationRoutes);

// Catch all endpoint
app.all("*", (req, res, next) => {
  logger.log("catch-all endpoint called");
  next({ message: "Endpoint does not exist", errorCode: 401 });
});

// Error handler
app.use("*", (error, req, res, next) => {
  logger.log("Errorhandler called!", error);

  res.status(error.errorCode).json({
    error: "Some error occured",
    message: error.message,
  });
});

app.listen(port, () => {
  logger.log(`Example app listening at http://localhost:${port}`);
});

module.exports = app;
