const { request, assert } = require("chai");
const chai = require("chai");
const chaiHttp = require("chai-http");
chai.should();
chai.use(chaiHttp);
const logger = require("tracer").console();
const server = require("../server");
const Developer = require("../src/entities/developer");
const Game = require("../src/entities/game");
const User = require("../src/entities/user");

const sampleUserData = {
  username: "Justinrds2001",
  password: "password",
};

const sampleDev = {
  name: "Test company",
  headquartersLocation: "The Netherlands",
  dateOfEstablishment: new Date(),
  founders: ["Justin"],
  website: new URL("https://www.bol.com/"),
  createdBy: undefined,
};

const sampleGame = {
  name: "Test Game",
  description: "daskdjhb alhksdbvsakhdklshavdb lkhsadasdad",
  releaseDate: new Date(),
  tags: ["tag"],
  developer: undefined,
  createdBy: undefined,
};

let user;
let dev;
let game;

describe("GameController tests", () => {
  beforeEach((done) => {
    user = new User(sampleUserData);
    user.save().then(() => {
      const devData = sampleDev;
      devData.createdBy = user;
      dev = new Developer(devData);
      dev.save().then(() => {
        const gameData = sampleGame;
        gameData.createdBy = user;
        gameData.developer = dev;
        game = new Game(gameData);
        done();
      });
    });
  });

  it("should create a game", (done) => {
    const gameToAdd = sampleGame;
    gameToAdd.createdBy = user;
    gameToAdd.developer = dev;
    chai
      .request(server)
      .post("/api/game")
      .send(gameToAdd)
      .end((err, res) => {
        assert.ifError(err);
        res.should.have.status(200);
        res.body.should.be
          .an("object")
          .that.has.all.keys("status", "addedGame");
        let { status, addedGame } = res.body;
        status.should.be.a("string").that.equals("successful");
        addedGame.should.be.an("object");
        addedGame.name.should.be.a("string").that.equals("Test Game");
        done();
      });
  });
});
