const mongoose = require("mongoose");

mongoose.Promise = global.Promise;

before((done) => {
  var mongoDBTest =
    "mongodb+srv://Justinrds2001:Origami0303@games-database.hj0rp.mongodb.net/mongoGameDatabaseTest?retryWrites=true&w=majority";
  mongoose.connect(mongoDBTest);
  mongoose.connection
    .once("open", () => {
      done();
    })
    .on("error", (error) => {
      console.warn("Warning", error);
    });
});

beforeEach((done) => {
  const { games, developers, articles } = mongoose.connection.collections;
  games.drop(() => {
    developers.drop(() => {
      articles.drop(() => {
        done();
      });
    });
  });
});
